package com.example.appx0d

import android.content.AbstractThreadedSyncAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SimpleAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "students"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ADDRESS = "address"
    val F_PHONE = "phone"
    val docID =""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e != null) Log.d("fireStore",e.message)
            showData()
        }
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{

            }
            R.id.btnUpdate ->{

            }
            R.id.btnDelete ->{

            }
        }

    }
    fun showDate(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for(doc in result){
                val hm = HashMap<String, String>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME,doc.get(F_NAME).toString())
                hm.set(F_ADDRESS,doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE,doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this, alStudent, R.layout.row_data,
            arrayOf(F_ID, F_NAME,F_ADDRESS,F_PHONE),
            intArrayOf(R.id.txld, R.id.txName, R.id.txAddress, R.id.txPhone))
            lsData.adapter = adapter
        }
    }
}